#!/usr/bin/env python3

import gitlab
import sys
import pprint

GITLAB_URL = "https://gitlab.torproject.org/"
PRIVATE_TOKEN = ""

gitlab_client = gitlab.Gitlab(GITLAB_URL, PRIVATE_TOKEN, api_version=4)

gitlab_client.auth()

user = gitlab_client.user
print("Logged in as: {}".format(user.username))

projects = set()

legacy_project = None

for project in gitlab_client.projects.list(all=True):
    if project.path_with_namespace.startswith("tpo/"):
        projects.add(project)
        continue

    if project.path_with_namespace == "legacy/trac":
        assert legacy_project is None
        legacy_project = project
        continue

issue_map = {}

for project in projects:
    print("Fetching issues from {}".format(project.path_with_namespace))

    for issue in project.issues.list(all=True):
        assert issue.id not in issue_map
        issue_map[issue.id] = issue

print("Fetching legacy issues")
with open("issues.csv", "w") as f:
    for issue in legacy_project.issues.list(all=True):
        print("Handling issue: {} ({})".format(issue.iid, issue.moved_to_id))

        # This issue has not been moved forward
        if issue.moved_to_id is None:
            f.write("{};{}\n".format(issue.iid, issue.web_url))
            continue

        moved_issue = None
        moved_to_id = issue.moved_to_id

        while moved_to_id is not None:
            moved_issue = issue_map[moved_to_id]
            moved_to_id = moved_issue.moved_to_id

        f.write("{};{}\n".format(moved_issue.iid, moved_issue.web_url))
