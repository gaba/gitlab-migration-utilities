This script builds a map from the legacy/trac project's issue IID's to their
final project destination after they have been moved out of legacy/trac.

This script was used to solve:
https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/12
